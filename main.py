from fastapi import BackgroundTasks, FastAPI
from time import time
import json, json5
import httpx
import asyncio

app = FastAPI()
job = "https://www2.stanzadelcittadino.it"


async def request(client, url):
    response = await client.get(url)
    return response.json()


async def process(json, client, metrics):
    labels = json["labels"]
    tenant = labels["__metrics_path__"]
    tenant = tenant.replace("/metrics", "")
    tenant = tenant.replace("/", "")
    r = await request(client, f"{job}/{tenant}/api/services")
    r2 = await client.get(f"{job}/{tenant}/metrics")
    metrics_text = r2.text
    metrics_text = metrics_text.split("\n")
    services = {}
    for line in metrics_text:
        if "sdc_applications{" in line:
            line = line.replace("sdc_applications", "")
            parentesis_pos = line.find("}")
            line_end = line[parentesis_pos + 2:]
            line = line[:parentesis_pos + 1]
            line = line.replace("=", ":")
            line = json5.loads(line)
            if line["status"] == "7000":
                services[line["service"]] = line_end
    for j in r:
        status = j["status"]
        payment_required = j["payment_required"]
        topic = j["topics"]
        name = j["name"]
        slug = j["slug"]
        flow_steps = j["flow_steps"]
        if flow_steps is not None:
            if name not in metrics:
                metrics[name] = {
                    tenant: {"status": status, "payment_required": payment_required, "topic": topic, "slug": slug,
                             "count": int(services[slug]) if slug in services else 0}}
            else:
                metrics[name][tenant] = {"status": status, "payment_required": payment_required, "topic": topic,
                                         "slug": slug, "count": int(services[slug]) if slug in services else 0}


async def test_process(tenant, client, metrics):
    r = await request(client, f"{job}/{tenant}/api/services")
    r2 = await client.get(f"{job}/{tenant}/metrics")
    metrics_text = r2.text
    metrics_text = metrics_text.split("\n")
    services = {}
    for line in metrics_text:
        if "sdc_applications{" in line:
            line = line.replace("sdc_applications", "")
            parentesis_pos = line.find("}")
            line_end = line[parentesis_pos + 2:]
            line = line[:parentesis_pos + 1]
            line = line.replace("=", ":")
            line = json5.loads(line)
            if line["status"] == "7000":
                services[line["service"]] = line_end
    for j in r:
        status = j["status"]
        payment_required = j["payment_required"]
        topic = j["topics"]
        name = j["name"]
        slug = j["slug"]
        flow_steps = j["flow_steps"]
        if flow_steps is not None:
            if name not in metrics:
                metrics[name] = {
                    tenant: {"status": status, "payment_required": payment_required, "topic": topic, "slug": slug,
                             "count": int(services[slug]) if slug in services else 0}}
            else:
                metrics[name][tenant] = {"status": status, "payment_required": payment_required, "topic": topic,
                                         "slug": slug, "count": int(services[slug]) if slug in services else 0}


async def get_data():
    client = httpx.AsyncClient()
    start = time()
    result = await request(client, "https://www2.stanzadelcittadino.it/prometheus.json")
    metrics = {}
    tasks = [process(i, client, metrics) for i in result]
    await asyncio.gather(*tasks)
    f = open("results.json", "w")
    f.write(json.dumps(metrics))
    print("time: ", time() - start)

@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/health")
async def root():
    return {"message": "alive :)"}


@app.get("/show-data")
async def show_data():
    file = open("results.json", "r")
    data = file.read()
    json_data = json.loads(data)
    return json_data


@app.get("/metrics")
async def metrics(BT: BackgroundTasks):
    BT.add_task(get_data)
    return {"message": "ok"}


@app.get("/test")
async def metrics():
    client = httpx.AsyncClient()
    start = time()
    result = await request(client, "https://www2.stanzadelcittadino.it/prometheus.json")
    metrics = {}
    tasks = [process(i, client, metrics) for i in result]
    await asyncio.gather(*tasks)
    print("time: ", time() - start)
    return metrics


@app.get("/test2")
async def metrics():
    client = httpx.AsyncClient()
    start = time()

    metrics = {}
    await test_process("comune-di-bugliano", client, metrics)
    print("time: ", time() - start)
    return metrics
