FROM python:3.10

RUN pip install --upgrade pip
WORKDIR code

COPY requirements.txt /code/requirements.txt

RUN pip install -r /code/requirements.txt

COPY main.py /code/main.py

CMD ["uvicorn", "main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "80"]
